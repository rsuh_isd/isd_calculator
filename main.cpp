#include <iostream>
#include "term.h"
#include <vector>
#include <sstream>
#include <algorithm>
#include <cmath>
using namespace std;

const int NO_PRIOR=0;
    const int PLUS_PRIOR=1;
    const int DIV_PRIOR=2;
    const int TERM_PRIOR=3;

void erroneous_expression(vector<Term*> &txt, size_t pos)
{
    cerr << "Error! Cannot parse this: " << txt[pos-1]->text() << txt[pos]->text() << txt[pos+1]->text() << ""<< endl;
    txt[pos]=new Error(txt[pos]);
}

void printExpression(vector<Term*> txt, vector<int> priors, bool withPriorities=false)
{
    clog << endl << "Printing text of math expression and operations' priorities" << endl;
    for (size_t i=0; i< txt.size(); i++) cout << txt[i]->text() << " ";
    cout << endl;
    for (size_t i=0; i< priors.size(); i++) {cout << priors[i];for (int j=(priors[i] == 0 ? 1 : (int)(log10(priors[i])+1)); j<=txt[i]->text().length();j++) cout << " ";}
    cout << endl;
    cout << endl;
}

int parseADivision(vector<Term*> &txt, vector<int> &priors, int i)
{
    Term * l, * r;
    if (priors[i-1]==NO_PRIOR)//lhs is a number
    {
        clog << "lhs is a number" << endl;
        istringstream fs(txt[i-1]->text());
        int a;
        fs >> a;
        l=new Number(a);
    }
    else if (priors[i-1]==TERM_PRIOR)
    {
        clog << "lhs is a term, appending it" << endl;
        l=txt[i-1];
    }
    else
    {
        erroneous_expression(txt, i);
        return 1;
    }

    if (priors[i+1]==NO_PRIOR)//rhs is a number
    {//WARNING: one could think rhs would always be a number, but what if there's power or some other higher operation?
        istringstream fs(txt[i+1]->text());
        int a;
        fs >> a;
        r=new Number(a);
    }
    else if (priors[i+1]==TERM_PRIOR)
    {
        clog << "rhs is a term, appending it" << endl;
        r=txt[i+1];
    }
    else
    {
        erroneous_expression(txt, i);
        return 1;
    }

    Term * d=new Division(l, r);

    txt[i]=d;

    clog << "new term appended, cleaning up terms it was constructed from" << endl;
    txt.erase(txt.begin()+i+1);
    txt.erase(txt.begin()+i-1);
    priors[i-1]=TERM_PRIOR;
    priors.erase(priors.begin()+i);
    priors.erase(priors.begin()+i);
    return 0;
}

int main()
{
    vector<Term*> txt;
    txt.push_back(new Text("3"));
    txt.push_back(new Text("+"));
    txt.push_back(new Text("12"));
    txt.push_back(new Text("/"));
    txt.push_back(new Text("3"));
    txt.push_back(new Text("/"));
    //txt.push_back(new Text("0"));
    txt.push_back(new Text("+"));
    txt.push_back(new Text("4"));
    txt.push_back(new Text("/"));
    txt.push_back(new Text("2"));
    vector<int> priors;
    priors.resize(txt.size(), NO_PRIOR);
    for (size_t i=0; i<txt.size(); i++)
    {
        if (txt[i]->text()=="+") priors[i]=PLUS_PRIOR;
        else if (txt[i]->text()=="/") priors[i]=DIV_PRIOR;
    }

    printExpression(txt,priors,true);

    clog << "parsing divisions" << endl;
    for (size_t i=0; i<txt.size(); i++)
    {
        if (priors[i]==2)
        {
            if (parseADivision(txt, priors, i) == 0) i-=2;//expression successfully shortened
        }
    }

    printExpression(txt, priors, true);

    txt[0]->prefix_tree_print();
    cout << endl;
    txt[2]->prefix_tree_print();
    cout << endl << txt[2]->value() << endl;

    return 0;
}
