char SPACE=' ';

class Term
{
public:
    //template<typename t>
    virtual float value()=0;
    virtual std::string text()=0;
    virtual void prefix_tree_print(size_t depth=0)=0;
    //virtual ~Term()=0;
};

class Text:public Term
{
private:
    std::string s_text;
    Text() {};
public:
    Text(std::string s):s_text(s) {};
    float value() {return 0;};
    std::string text() {return s_text;};
    void prefix_tree_print(size_t depth=0) {std::cout<<std::string(depth, SPACE)<<"\"" << s_text<<"\""<<std::endl;};
};

class Number:public Term
{
private:
    float f_value;
    Number() {};
public:
    Number(float v):f_value(v) {};
    float value() {return f_value;};
    std::string text() {return "";};
    void prefix_tree_print(size_t depth=0)
    {
        std::cout<<std::string(depth, SPACE);
        std::cout<<"i:" << value()<<""<<std::endl;
    };
};

class Sum:public Term
{
private:
    Term * l;
    Term * r;
    Sum(const Sum & orig) {};//TODO: make copies of both arguments
    Sum () {};
public:
    //Sum() {l=new Number(6); r=new Number(3);};
    Sum(Term * t1, Term * t2):l(t1), r(t2) {};
    float value() {return l->value()+r->value();};
  std::string text() {return "";};
  void prefix_tree_print(size_t depth=0) {std::cout<<std::string(depth, SPACE)<<"+"<<std::endl;l->prefix_tree_print(depth+1);r->prefix_tree_print(depth+1);};
};

class Division:public Term
{
    private:
    Term * l;
    Term * r;
    Division(const Division & orig) {};//TODO: make copies of both arguments
    Division() {};
public:
    Division(Term * t1, Term * t2):l(t1), r(t2) {};
    float value() {float rval=r->value(); if (rval!=0) return l->value()/r->value(); std::cerr << "Division by zero! Making the result a 0" << std::endl; return 0;};
    std::string text() {return "ITS A DIVISION!";};
    void prefix_tree_print(size_t depth=0) {std::cout<<std::string(depth, SPACE)<<"/"<<std::endl;l->prefix_tree_print(depth+1);r->prefix_tree_print(depth+1);};
};

class Parenthesis:public Term
{
private:
    Term * contens;
    Parenthesis(const Parenthesis & orig) {};//TODO: make copies of both arguments
public:
    Parenthesis(Term * t):contens(t) {};
    float value() {contens->value();};
std::string text() {return "";};
void prefix_tree_print(size_t depth=0) {std::cout<<std::string(depth, SPACE)<<"()"<<std::endl;contens->prefix_tree_print(depth+1);};
};

class Error:public Parenthesis
{
    public:
    Error(Term * t):Parenthesis(t) {};
    float value() {std::cerr << "Cannot evaluate an erroneous expression, assuming its value is 0." << std::endl; return 0;}
};
